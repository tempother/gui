﻿Add-Type -AssemblyName system.windows.forms

function new-Form {
    Param (
    [String] $Form_Title, [Int] $Form_X, [int] $Form_y, [int] $Form_W, [int] $Form_H, [String] $back="LightGray",[String] $fore="Black")

    $Object_Form = New-Object System.Windows.Forms.Form
    $Object_Form.Location = New-Object System.Drawing.Point($Form_X,$Form_y)
    $Object_Form.Size = New-Object System.Drawing.Size($Form_W,$Form_H)
    $Object_Form.Text = $Form_Title   
    $Object_Form.BackColor=$back
    $Object_Form.ForeColor=$fore
    Return $Object_Form
}

Function New-Button {
    Param ( [String] $Button_Text, [Int] $Button_X, [int] $Button_y, [int] $Button_W, [int] $Button_H, [Object] $Form_post, [String] $back="LightGray",[String] $fore="Black")

        $Object_Button = New-Object System.Windows.Forms.Button
        $Object_Button.Text = $Button_Text
        $Object_Button.Location = New-Object System.Drawing.Point($Button_X,$Button_y)
        $Object_Button.Size = New-Object System.Drawing.Size($Button_W,$Button_H)
        $Object_Button.BackColor=$back
        $Object_Button.ForeColor=$fore
        $Object_Button.DialogResult = [System.Windows.Forms.DialogResult]::None #stops powershell from closing the form
        $Form_post.Controls.Add($Object_Button)
        Return $Object_Button
}

Function New-Label {
    Param ( [String] $Label_Text, [Int] $Label_X, [int] $Label_y, #[int] $Label_W, [int] $Label_H,
     [Object] $Form_post, [String] $back="White",[String] $fore="Black")

        $Object_Label = New-Object System.Windows.Forms.Label
        $Object_Label.Text = $Label_Text
        $Object_Label.Location = New-Object System.Drawing.Point($Label_X,$Label_y)
        $Object_Label.AutoSize=$true
        #$Object_Label.MultiLine=$true
        $Object_Label.BackColor=$back
        $Object_Label.ForeColor=$fore
        $Form_post.Controls.Add($Object_Label)
        Return $Object_Label
}

Function New-Combo {
    Param ( [String[]] $Combo_Text, [Int] $Combo_X, [int] $Combo_y, [int] $Combo_W, [Object] $Form_post, [String] $back="White",[String] $fore="Black")

        $Object_Combo = New-Object System.Windows.Forms.ComboBox
        $Object_Combo.Width = $Combo_W
        foreach ($x in $Combo_Text) 
        {
            $Object_Combo.Items.Add($x)
        }
        $Object_Combo.Location = New-Object System.Drawing.Point($Combo_X,$Combo_y)
        $Object_Combo.BackColor=$back
        $Object_Combo.ForeColor=$fore

        $Object_Combo.DropDownStyle = [System.Windows.Forms.ComboBoxStyle]::DropDownList
        $Form_post.Controls.Add($Object_Combo)
        Return $Object_Combo
}

Function New-Checkbox {
    Param ( [String] $Check_Text, [Int] $Check_X, [int] $Check_y, [int] $Check_W, [int] $Check_H, [Object] $Form_post, [String] $back="White",[String] $fore="Black")
        $Object_Check = New-Object System.Windows.Forms.Checkbox
        $Object_Check.Location = New-Object System.Drawing.Point($Check_X,$Check_y)
        $Object_Check.Size = New-Object System.Drawing.Size($Check_W,$Check_H)
        $Object_Check.BackColor=$back
        $Object_Check.ForeColor=$fore
        $Object_Check.Text = $Check_Text
        $Form_post.Controls.Add($Object_Check)
        Return $Object_Check
}

Function New-Textbox {
    Param ( [String] $Textbox_Text, [Int] $Textbox_X, [int] $Textbox_y, [int] $Textbox_W, [int] $Textbox_H, [Object] $Form_post, [String] $back="White",[String] $fore="Black")

        $Object_Textbox = New-Object System.Windows.Forms.Textbox
        $Object_Textbox.Text = $Textbox_Text
        $Object_Textbox.Location = New-Object System.Drawing.Point($Textbox_X,$Textbox_y)
        $Object_Textbox.Size = New-Object System.Drawing.Point($Textbox_W,$Textbox_H)
        $Object_Textbox.BackColor=$back
        $Object_Textbox.ForeColor=$fore
        $Object_Textbox.Multiline=$true
        $Form_post.Controls.Add($Object_Textbox)
        Return $Object_Textbox
}

Function New-Listbox {
    Param ( [String[]] $List_Text, [Int] $List_X, [int] $List_y, [int] $List_W, [Object] $Form_post, [String] $back="White",[String] $fore="Black")

        $Object_List = New-Object System.Windows.Forms.Listbox
        $Object_List.Location = New-Object System.Drawing.Point($List_X,$List_y)
        $List_H=$List_Text.Count*20
        $Object_List.Size = New-Object System.Drawing.Size($List_W,$List_H)
        $Object_List.BackColor=$back
        $Object_List.ForeColor=$fore
        $Object_List.SelectionMode = 'MultiExtended'

        $Object_List.Width = $List_W
        foreach ($x in $List_Text) 
        {
            $Object_List.Items.Add($x)
        }
        
        $Form_post.Controls.Add($Object_List)
        Return $Object_List
}

Function Error_box {
Param (
    [String] $Error_msg, [String] $back="Red",[String] $fore="Black")
    $Error = New-Form "Error" 1500 1500 500 500 $back
    $login= New-Label $Error_msg 100 10 $Error 
    $login.Font=New-Object System.Drawing.Font("Times New Roman",16,[System.Drawing.FontStyle]::Bold)

    $ok = New-Button "Ok" 200 350 100 50 $Error    
    $ok.Font=New-Object System.Drawing.Font("Times New Roman",16,[System.Drawing.FontStyle]::Bold)
    $Ok.DialogResult = [System.Windows.Forms.DialogResult]::Ok
    
    $Error.Add_Shown({$FMain.Activate()})
    $Error.ShowDialog() 
}



Function New-TabControl {
    Param ( [String] $TabControl_Text, [Int] $TabControl_X, [int] $TabControl_y, [int] $TabControl_W, [int] $TabControl_H, [Object] $Form_post, 
    [String] $back="White",[String] $fore="Black")
    $TabControl= New-Object System.Windows.Forms.TabControl
    $TabControl.Location = New-Object System.Drawing.Point($TabControl_X,$TabControl_y)
    $TabControl.Size = New-Object System.Drawing.Point($TabControl_W,$TabControl_H)
    $TabControl.Name= $TabControl_Text
    $Form_post.Controls.Add($TabControl)
    Return $TabControl
}

Function New-TabOption {
    Param ([String] $Tabname, [Object] $TabControl)
        $TabPage = New-Object System.Windows.Forms.TabPage 
        $TabPage.Text = $Tabname
        $TabPage.Name = $Tabname
        #$TabPage.Controls.Add(new Button());
        $TabControl.Controls.Add($TabPage)
        Return $TabPage
    }