﻿#Add-Type -AssemblyName system.windows.forms

. "$PSScriptRoot\gui_builder.ps1"

Function makeForm {
    #----------------------------------
    #This is setting the layout of the form
    #----------------------------------

    $FMain = New-Form "GUI builder example 0.1" 500 500 1000 500

    $login= New-Label "This is a basic label, below is a Checkbox" 100 10 $FMain "LightGreen"
    $check = New-Checkbox "Checkbox box" 100 50 100 30 $FMain
    
    $label2= New-Label "new" 100 100 $FMain "LightGreen"
    $textbox= New-Textbox "Textbox default text" 100 120 200 50 $FMain
    
    $label3= New-Label "Combo box" 100 200 $FMain "LightGreen"
    $val = @("a","b","c","d")
    $combo=New-Combo $val 100 230 100 $FMain

    $label3= New-Label "Pick multiple values of combo" 100 280 $FMain "LightGreen"
    $list= New-Listbox @("x","y") 100 300 50 $FMain

    $res_box = New-Textbox "Results box" 600 10 380 445 $FMain 

    $label2= New-Label "Button to get items" 300 350 $FMain "LightGreen"
    $ok = New-Button "Submit" 300 370 100 50 $FMain "Red"
    
    $ok.Font=New-Object System.Drawing.Font("Times New Roman",16,[System.Drawing.FontStyle]::Bold)
        # Font styles are: Regular, Bold, Italic, Underline, Strikeout

    $tabControl = New-TabControl "mainTab" 400 10 200 25 $FMain
    $firstTab = New-TabOption "first" $tabControl
    $secTab = New-TabOption "sec" $tabControl

    #------------------------------------
    #setting actions
    #------------------------------------

    #really what you would want to do is take the values from the screen and then pass them to a seperate function
    $ok.Add_Click({
            if($check.Checked -eq $true) { 
                $out=""
                $out=-join($out,"checked","`r`n") 
                if($combo.SelectedItem -ne $null) { $out=-join($out,$combo.SelectedItem,"`r`n") } #if we have made a selection from the drop down, output it otherwise dont
                if($list.SelectedItem -ne $null) { 
                    foreach($x in $list.SelectedItems) {
                        $out=-join($out,$x,"`r`n")  #if we have made a selection from the drop down, output it otherwise dont
                    }
                }
                $res_box.Text=$out
            } else {Error_box "Tickbox is not checked"}
            
            $FMain.Refresh()
        })

    #-------------------------


    #----------------------

    #$res_box.AutoSize = $true
    $FMain.Add_Shown({$FMain.Activate()})
    $FMain.ShowDialog()
}

makeForm

